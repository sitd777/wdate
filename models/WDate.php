<?php

namespace app\models;

class WDate
{
    /**
     * Holds hours part
     * @var null || integer
     */
    public $hours = null;

    /**
     * Holds minutes part
     * @var null || integer
     */
    public $minutes = null;

    /**
     * Holds seconds part
     * @var null || integer
     */
    public $seconds = null;

    /**
     * Holds day part
     * @var null || integer
     */
    public $day = null;

    /**
     * Holds month part
     * @var null || integer
     */
    public $month = null;

    /**
     * Holds year part
     * @var null || integer
     */
    public $year = null;

    /**
     * WDate constructor.
     * @param $stringDate
     */
    public function __construct($stringDate)
    {
        // Prepare string
        $stringDate = trim(preg_replace('/\s{1,}/', ' ', $stringDate));

        // Explode to time and date
        $explode = explode(' ', $stringDate, 2);

        // Explode time and date data
        $time = $date = [];
        if (sizeof($explode) == 2)
        {
            $time = explode(':', $explode[0]);
            $date = explode('.', $explode[1]);
        } else if(strpos($explode[0], '.') === false && strpos($explode[0], ':') === false) {
            if(strlen($explode[0]) == 4) $this->year = (int)$explode[0];
            else $this->day = (int)$explode[0];
        } else {
            if(strpos($explode[0], '.') !== false) $date = explode('.', $explode[0]);
            else $time = explode(':', $explode[0]);
        }

        // Set time parts
        if(isset($time[0]) && $time[0] != '') $this->hours   = (int)$time[0];
        if(isset($time[1]) && $time[1] != '') $this->minutes = (int)$time[1];
        if(isset($time[2]) && $time[2] != '') $this->seconds = (int)$time[2];

        // Set date parts
        if(sizeof($date) > 0 && sizeof($date) < 3 && strlen($date[sizeof($date)-1]) == 4)
        {
            $this->year  = (int)array_pop($date);
            if(sizeof($date)) $this->month = (int)array_pop($date);
        } else {
            if(isset($date[0]) && $date[0] != '') $this->day     = (int)$date[0];
            if(isset($date[1]) && $date[1] != '') $this->month   = (int)$date[1];
            if(isset($date[2]) && $date[2] != '') $this->year    = (int)$date[2];
        }
    }

    /**
     * Converts current object to string
     * @return string
     */
    public function __toString()
    {
        return
            (!is_null($this->hours)   ? sprintf("%02d", $this->hours)   : '') . ':' .
            (!is_null($this->minutes) ? sprintf("%02d", $this->minutes) : '') . ':' .
            (!is_null($this->seconds) ? sprintf("%02d", $this->seconds) : '') . ' ' .
            (!is_null($this->day)     ? sprintf("%02d", $this->day)     : '') . '.' .
            (!is_null($this->month)   ? sprintf("%02d", $this->month)   : '') . '.' .
            (!is_null($this->year)    ? $this->year                     : '');
    }

    /**
     * Compares two WDate objects
     * @param  WDate $obj1
     * @param  WDate $obj2
     * @return int
     */
    public static function compare(WDate $obj1, WDate $obj2)
    {
        // Compare years
        if(!is_null($obj1->year) && !is_null($obj2->year) && $obj1->year != $obj2->year) return ($obj1->year > $obj2->year) ? 1 : -1;

        // Compare months
        if(!is_null($obj1->month) && !is_null($obj2->month) && $obj1->month != $obj2->month) return ($obj1->month > $obj2->month) ? 1 : -1;

        // Compare days
        if(!is_null($obj1->day) && !is_null($obj2->day) && $obj1->day != $obj2->day) return ($obj1->day > $obj2->day) ? 1 : -1;

        // Compare hours
        if(!is_null($obj1->hours) && !is_null($obj2->hours) && $obj1->hours != $obj2->hours) return ($obj1->hours > $obj2->hours) ? 1 : -1;

        // Compare minutes
        if(!is_null($obj1->minutes) && !is_null($obj2->minutes) && $obj1->minutes != $obj2->minutes) return ($obj1->minutes > $obj2->minutes) ? 1 : -1;

        // Compare seconds
        if(!is_null($obj1->seconds) && !is_null($obj2->seconds) && $obj1->seconds != $obj2->seconds) return ($obj1->seconds > $obj2->seconds) ? 1 : -1;

        return 0;
    }

    /**
     * Compares current object with another
     * @param  WDate $obj
     * @return int
     */
    public function equals(WDate $obj)
    {
        return static::compare($this, $obj);
    }
}
