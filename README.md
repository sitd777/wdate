# WDate #

WDate class test job

Created using Yii2 framework

### Class ###

* [WDate](https://bitbucket.org/sitd777/wdate/src/53fc8ed658e1a730501f5156a6c02727cada4e9c/models/WDate.php?at=master)

### Usage ###
Works with next versions of date string:
```
"01:02:03 04.05.2017"
"01:02 04.05.2017"
"01 04.05.2017"
":02:03 04.05.2017"
"::03 04.05.2017"
":02: 04.05.2017"
"01:02:03 05.2017"
"01:02:03 04..2017"
"01:02:03 04.05"
"01:02:03 04"
"01:02:03 2017"

"01:02:03"
"01:02"
"01:"
":02:03"
"::03"
":02:"

"04.05.2017"
"05.2017"
"04..2017"
"04.05"
"04."
"2017"
```

### WDate comparison examples: ###

```
WDate::compare($date1, $date2);
```
OR
```
$date1->equals($date2);
```

### Tests: ###

* [WDate date and time test](https://bitbucket.org/sitd777/wdate/src/53fc8ed658e1a730501f5156a6c02727cada4e9c/tests/unit/models/WDateDateTimeTest.php?at=master)
* [WDate only date test](https://bitbucket.org/sitd777/wdate/src/53fc8ed658e1a730501f5156a6c02727cada4e9c/tests/unit/models/WDateDateTest.php?at=master)
* [WDate only time test](https://bitbucket.org/sitd777/wdate/src/53fc8ed658e1a730501f5156a6c02727cada4e9c/tests/unit/models/WDateTimeTest.php?at=master)
* [WDate comparison test](https://bitbucket.org/sitd777/wdate/src/6f1dcc0050cb84f00ee9df037b562ebc73df5015/tests/unit/models/WDateComparisonTest.php?at=master)

### Tests screenshots: ###

* [Screenshot #1](https://bitbucket.org/sitd777/wdate/src/6f1dcc0050cb84f00ee9df037b562ebc73df5015/screenshot.png?at=master)

### Tests execution command: ###

```
vendor\bin\codecept run
```
