<?php

namespace tests\models;

use app\models\WDate;

class WDateComparisonTest extends \Codeception\Test\Unit
{
    public function testCompareWdateV1()
    {
        $date1 = new WDate("01:02:03 04.05.2017");
        $date2 = new WDate("01:02:03 04.05.2018");

        expect_that($date1->equals($date2) < 0);
    }

    public function testCompareWdateV2()
    {
        $date1 = new WDate("01:02:03 04.06.2017");
        $date2 = new WDate("01:02:03 04.05.");

        expect_that($date1->equals($date2) > 0);
    }

    public function testCompareWdateV3()
    {
        $date1 = new WDate("01:02:03 04..2017");
        $date2 = new WDate("01:02:03 04.05.");

        expect_that($date1->equals($date2) == 0);
    }

    public function testCompareWdateV4()
    {
        $date1 = new WDate("01:02:03 04.05.2017");
        $date2 = new WDate("01:02:03");

        expect_that($date1->equals($date2) == 0);
    }

    public function testCompareWdateV5()
    {
        $date1 = new WDate("01:02:03 04.05.2017");
        $date2 = new WDate("01:02:04");

        expect_that($date1->equals($date2) < 0);
    }

    public function testCompareWdateV6()
    {
        $date1 = new WDate("01:02:03 04.05.2017");
        $date2 = new WDate("01:03");

        expect_that(WDate::compare($date1, $date2) < 0);
    }

    public function testCompareWdateV7()
    {
        $date1 = new WDate("02:02:03 04.05.2017");
        $date2 = new WDate("01:03");

        expect_that(WDate::compare($date1, $date2) > 0);
    }
}
