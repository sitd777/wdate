<?php

namespace tests\models;

use app\models\WDate;

class WDateTimeTest extends \Codeception\Test\Unit
{
    public function testParseWdateV1()
    {
        $date = new WDate("01:02:03");

        expect_that($date->hours == 1);
        expect_that($date->minutes == 2);
        expect_that($date->seconds == 3);
        expect_that(is_null($date->day));
        expect_that(is_null($date->month));
        expect_that(is_null($date->year));
    }

    public function testParseWdateV2()
    {
        $date = new WDate("01:02");

        expect_that($date->hours == 1);
        expect_that($date->minutes == 2);
        expect_that(is_null($date->seconds));
        expect_that(is_null($date->day));
        expect_that(is_null($date->month));
        expect_that(is_null($date->year));
    }

    public function testParseWdateV3()
    {
        $date = new WDate("01:");

        expect_that($date->hours == 1);
        expect_that(is_null($date->minutes));
        expect_that(is_null($date->seconds));
        expect_that(is_null($date->day));
        expect_that(is_null($date->month));
        expect_that(is_null($date->year));
    }

    public function testParseWdateV4()
    {
        $date = new WDate(":02:03");

        expect_that(is_null($date->hours));
        expect_that($date->minutes == 2);
        expect_that($date->seconds == 3);
        expect_that(is_null($date->day));
        expect_that(is_null($date->month));
        expect_that(is_null($date->year));
    }

    public function testParseWdateV5()
    {
        $date = new WDate("::03");

        expect_that(is_null($date->hours));
        expect_that(is_null($date->minutes));
        expect_that($date->seconds == 3);
        expect_that(is_null($date->day));
        expect_that(is_null($date->month));
        expect_that(is_null($date->year));
    }

    public function testParseWdateV6()
    {
        $date = new WDate(":02:");

        expect_that(is_null($date->hours));
        expect_that($date->minutes == 2);
        expect_that(is_null($date->seconds));
        expect_that(is_null($date->day));
        expect_that(is_null($date->month));
        expect_that(is_null($date->year));
    }
}
