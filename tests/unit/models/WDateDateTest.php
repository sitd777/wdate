<?php

namespace tests\models;

use app\models\WDate;

class WDateDateTest extends \Codeception\Test\Unit
{
    public function testParseWdateV1()
    {
        $date = new WDate("04.05.2017");

        expect_that(is_null($date->hours));
        expect_that(is_null($date->minutes));
        expect_that(is_null($date->seconds));
        expect_that($date->day == 4);
        expect_that($date->month == 5);
        expect_that($date->year == 2017);
    }

    public function testParseWdateV2()
    {
        $date = new WDate("05.2017");

        expect_that(is_null($date->hours));
        expect_that(is_null($date->minutes));
        expect_that(is_null($date->seconds));
        expect_that(is_null($date->day));
        expect_that($date->month == 5);
        expect_that($date->year == 2017);
    }

    public function testParseWdateV3()
    {
        $date = new WDate("04..2017");

        expect_that(is_null($date->hours));
        expect_that(is_null($date->minutes));
        expect_that(is_null($date->seconds));
        expect_that($date->day == 4);
        expect_that(is_null($date->month));
        expect_that($date->year == 2017);
    }

    public function testParseWdateV4()
    {
        $date = new WDate("04.05");

        expect_that(is_null($date->hours));
        expect_that(is_null($date->minutes));
        expect_that(is_null($date->seconds));
        expect_that($date->day == 4);
        expect_that($date->month == 5);
        expect_that(is_null($date->year));
    }

    public function testParseWdateV5()
    {
        $date = new WDate("04.");

        expect_that(is_null($date->hours));
        expect_that(is_null($date->minutes));
        expect_that(is_null($date->seconds));
        expect_that($date->day == 4);
        expect_that(is_null($date->month));
        expect_that(is_null($date->year));
    }

    public function testParseWdateV6()
    {
        $date = new WDate("2017");

        expect_that(is_null($date->hours));
        expect_that(is_null($date->minutes));
        expect_that(is_null($date->seconds));
        expect_that(is_null($date->day));
        expect_that(is_null($date->month));
        expect_that($date->year == 2017);
    }
}
